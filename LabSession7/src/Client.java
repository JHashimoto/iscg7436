import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketImpl;
import java.net.SocketImplFactory;
import java.net.UnknownHostException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Client extends JFrame implements ActionListener, Runnable{
	
	private static final long serialVersionUID = 980389841528802556L;
	JTextField chatInput = new JTextField(50);
	JTextField handleInput = new JTextField(50);
	JTextArea chatHistory = new JTextArea(5,50);
	JButton chatMessage = new JButton("Send");
	JLabel labelChat = new JLabel("Chat:");
	JLabel labelHandle = new JLabel("Handle:");
	
	Socket client;
	DataInputStream dis;
	DataOutputStream dos;
	
	public Client()
	{
		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout());

		contentPane.add(labelChat,BorderLayout.WEST);
		contentPane.add(chatInput,BorderLayout.CENTER);
		contentPane.add(chatMessage,BorderLayout.EAST);
		contentPane.add(new JScrollPane(chatHistory),BorderLayout.NORTH);
		Panel panel = new Panel();
		contentPane.add(panel,BorderLayout.SOUTH);
		
		panel.add(labelHandle);
		panel.add(handleInput);
		
		pack();
		setVisible(true);
		
		chatHistory.setEditable(false);
		
		chatMessage.addActionListener(this);
		try {
			//client = new Socket("localhost",5000);
			client = new Socket(java.net.InetAddress.getLocalHost(), 5000);
			
			dis = new DataInputStream(client.getInputStream());
			dos = new DataOutputStream(client.getOutputStream());
			
			Thread clientThread = new Thread(this);
			clientThread.start();
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		Client client = new Client();
		client.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		int kind = 0;
		String msg = "";
		try {
			// Send a handle
			kind = ServerConstants.HANDLE;
			msg = handleInput.getText();
			dos.writeInt(kind);
			dos.writeUTF(msg);			
			dos.flush();
			// Send a chat message
			kind = ServerConstants.CHAT_MESSAGE;
			msg = chatInput.getText();
			dos.writeInt(kind);
			chatHistory.append(msg + "\n");
			dos.writeUTF(msg);			
			dos.flush();
		}
		catch (IOException e){
			e.printStackTrace();
		}
	}

	@Override
	public void run()
	{
		while(true)
		{
			try {
				int messageType = dis.readInt();
				
				switch(messageType)
				{
					case ServerConstants.CHAT_BROADCAST:
						chatHistory.append(dis.readUTF()+"\n");
						break;
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		
	}
}
