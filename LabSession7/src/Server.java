import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JButton;;

public class Server extends JFrame implements ActionListener
{
	private static final long serialVersionUID = -2291453973624020582L;
	ServerSocket serverSocket;
	JTextArea systemLog = new JTextArea(5,60);
	DefaultListModel<String> model;
	JList<String> handleList;
	JTextField clientMessage;
	JButton sendButton;
	ArrayList <ServerThread> connectedClients = new ArrayList<ServerThread>();
	
	public Server()
	{
		this.setTitle("Server");
		try {
			serverSocket = new ServerSocket(5000);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		// Set Swing components
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.add(new JScrollPane(systemLog),BorderLayout.CENTER);
		Panel panel = new Panel();
		contentPane.add(panel,BorderLayout.SOUTH);
		
		// Set JList component
		model = new DefaultListModel<String>();
		handleList = new JList<String>(model);
		clientMessage = new JTextField(30);
		sendButton = new JButton();
		sendButton.setText("Send");
		sendButton.addActionListener(this);
		
		panel.add(handleList);
		panel.add(clientMessage);
		panel.add(sendButton);
		
		pack(); 
		setVisible(true);
	}
	public void start()
	{
		try
		{
			while(true) // keep accepting new clients
			{ 
				Socket client = serverSocket.accept();
				ServerThread st = new ServerThread(client,this,connectedClients);
				st.start();
				connectedClients.add(st);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public static void main(String[] args)
	{
		Server server = new Server();
		server.start();
	}
	public JTextArea getSystemLog() {
		return systemLog;
	}
	public void setSystemLog(JTextArea systemLog) {
		this.systemLog = systemLog;
	}
	public void addHandle(String handle){
		int index = model.getSize();
		model.add(index, handle);		
	}
	public void updateHandle(String oldHandle, String newHandle){
		for (int i = 0; i < model.getSize(); i++){
			String handle = model.get(i);
			if (handle.equalsIgnoreCase(oldHandle)){
				model.set(i, newHandle);
			}
		}
	}
	@Override
	public void actionPerformed(ActionEvent event) {
		if (model.getSize() == 0) return;
		if (handleList.isSelectionEmpty()) return;
		String message = clientMessage.getText();
		for (int index : handleList.getSelectedIndices()){
			String handle = model.get(index);
			ServerThread client = findClient(handle);
			if (client == null) continue;
			client.sendMessageToClient(message);
		}
	}
	private ServerThread findClient(String handle){
		ServerThread client = null;
		for (ServerThread st : connectedClients){
			if (st.getHandle().equalsIgnoreCase(handle)){
				client = st;
				break;
			}
		}		
		return client;
	}
}
