import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;


public class ServerThread extends Thread {

	DataInputStream dis;
	DataOutputStream dos;
	
	Socket client;
	Server server;
	
	ArrayList<ServerThread> connectedClients;
	
	static Vector<ServerThread> threads;
	
	String handle;
	public ServerThread(Socket client, Server server, ArrayList<ServerThread> connectedClients)
	{
		super();
		this.client = client;
		this.connectedClients = connectedClients;
		try {
			this.dis = new DataInputStream(client.getInputStream());
			this.dos = new DataOutputStream(client.getOutputStream());
			this.server = server;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		if (threads == null){
			threads = new Vector<ServerThread>();
		}
		threads.add(this);
		handle = "client" + Integer.toString(threads.size());
		server.addHandle(handle);
	}
	public void run()
	{
		while(true)
		{
			try {
				int mesgType = dis.readInt();
				System.err.println(mesgType);
				switch(mesgType)
				{
					case ServerConstants.CHAT_MESSAGE:
						String data = dis.readUTF();
						System.err.println(data);
						//server.getSystemLog().append(client.getInetAddress()+":"+client.getPort()+">"+data+"\n");
						server.getSystemLog().append(handle+": "+">"+data+"\n");
						for(ServerThread otherClient: connectedClients)
						{
							if(!otherClient.equals(this))
							{
								otherClient.getDos().writeInt(ServerConstants.CHAT_BROADCAST);
								otherClient.getDos().writeUTF(data);
							}
						}
						break;
					case ServerConstants.HANDLE:
						String sentHandle = dis.readUTF();
						if ((sentHandle == null)||(sentHandle.isEmpty())) break;
						if (!handle.equalsIgnoreCase(sentHandle)){
							server.updateHandle(handle, sentHandle);
							handle = sentHandle;
						}
						break;
					default:
						break;
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
				return;
			}
		}
	}
	public void sendMessageToClient(String message){
		try {
			dos.writeInt(ServerConstants.CHAT_BROADCAST);
			dos.writeUTF(message);
			dos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public DataOutputStream getDos() {
		return dos;
	}
	public DataInputStream getDis() {
		return dis;
	}
	
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}

}
