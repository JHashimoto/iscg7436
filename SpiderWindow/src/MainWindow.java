
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.spider.*;

public class MainWindow extends JFrame 
implements ActionListener{
	private static final long serialVersionUID = 1L;

	// Web Crawler
	private SpiderController controller;	
	
	// Window Size
	private final int width = 500;
	private final int height = 400;
	
	private JComboBox<String> urlBox;
	private JTable table = new JTable();
	private String[] columnNames = {"Name", "URL"};
	private DefaultTableModel tModel;
	private JTextField terms;
	

	public MainWindow(){
		this.controller = SpiderController.getInstance();
		setSize(width, height);
		setTitle("ISCG7436 Assignment2");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		java.awt.Container cont = this.getContentPane();
		cont.setLayout(null);
		// set combobox
		JLabel urlLabel = new JLabel("URL:");
		cont.add(urlLabel);
		urlBox = new JComboBox<String>();
		urlBox.setEditable(true);		
		cont.add(urlBox);
		urlLabel.setBounds(0, 0, 50, 30);
		urlBox.setBounds(50, 0, 400, 30);
		
		// set search box
		JLabel searchLabel = new JLabel("Search:");
		cont.add(searchLabel);
		terms = new JTextField();		
		cont.add(terms);
		searchLabel.setBounds(0, 30, 50, 30);
		terms.setBounds(50, 30, 300, 30);
		JButton sButton = new JButton();
		sButton.setText("Search");
		sButton.addActionListener(this);
		cont.add(sButton);
		sButton.setBounds(350, 30, 100, 30);
		
		//set result table 
		JLabel resultLabel = new JLabel("Result:");
		cont.add(resultLabel);
		resultLabel.setBounds(0, 60, 50, 30);
		tModel = new DefaultTableModel(columnNames, 0);
		table = new JTable(tModel);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0).setPreferredWidth(50);
		table.getColumnModel().getColumn(1).setPreferredWidth(300);
		JScrollPane scroll = new JScrollPane(table);
		cont.add(scroll);
		scroll.setBounds(0, 90, (width-20), (height-90));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (controller == null) return;
		try {
			String url = getURI();
			if (url.isEmpty()) return;
			URIBuilder builder = new URIBuilder(url);
			controller.doCrawl(builder.build());
			clearRows();
			String terms = getTerms();
			if (terms != null && !terms.isEmpty()){
				for (com.model.Document doc : controller.search(terms)){
					addResult(doc);
				}
			}
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	private String getURI(){
		if (urlBox.getSelectedItem() == null) return "";
		return ((String)urlBox.getSelectedItem());
	}
	private String getTerms(){
		if (terms.getText() == null) return "";
		return terms.getText();
	}
	private void addResult(com.model.Document doc){
		tModel.addRow(new String[] {doc.getTitle(), doc.getUrl()});
	}
	private void clearRows(){
		for (int i = 0; i < tModel.getRowCount(); i++){
			tModel.removeRow(0);
		}
	}
	public static void main(String args[]){
		MainWindow window = new MainWindow();
		window.setVisible(true);
	}
}
