package com.jcasey.action;

import java.util.LinkedList;
import java.util.List;
import com.jcasey.controller.BookGenreManager;
import com.jcasey.controller.BookManager;
import com.jcasey.controller.GenreManager;
import com.jcasey.model.Book;
import com.jcasey.model.Genre;
import com.jcasey.model.BookGenre;
import com.opensymphony.xwork2.Action;

public class ModifyBook
{	
	private BookManager linkController;
	private GenreManager genreManager;
	private BookGenreManager bookGenreManager;
	
	private Long bookId;
	private String title;
	private String author;
	private String genre;
	private String isbn;
	private String blurb;
	
	public ModifyBook()
	{
		linkController = new BookManager();
		genreManager = new GenreManager();
		bookGenreManager = new BookGenreManager();
	}
	
	public String add()
	{
		Book book = new Book();
		
		book.setAuthor(author);
		book.setTitle(title);
		book.setBlurb(blurb);
		//book.setGenre(genre);
		book.setIsbn(isbn);

		linkController.add(book);
		// adding Genres
		List<Genre> genres = new LinkedList<Genre>();
		for (String genre : this.genre.split(",")){
			Genre existing = genreManager.findByGenre(genre);
			if (existing == null){
				genres.add(genreManager.add(new Genre(genre)));
			}else{
				genres.add(existing);
			}
		}
		// adding BookGenres
		for (Genre genre : genres){
			BookGenre bg = new BookGenre(book, genre);
			bookGenreManager.add(bg);
		}
		return Action.SUCCESS;
	}
	
	public String update()
	{
		// get the current book
		
		Book book = linkController.get(getBookId());
		
		// update the book setters based on new form data		
		book.setAuthor(author);
		book.setTitle(title);
		book.setBlurb(blurb);
		//book.setGenre(genre);
		book.setIsbn(isbn);
		
		// actually update the book using the linkController
		linkController.update(book);
		// delete all records of BookGenre
		for (BookGenre bg : bookGenreManager.findByBook(book)){
			bookGenreManager.delete(bg);
		}
		// recreate BookGenre records
		for (String genre : this.genre.split(",")){
			Genre existing = genreManager.findByGenre(genre);
			if (existing == null){
				existing = genreManager.add(new Genre(genre));
			}
			bookGenreManager.add(new BookGenre(book, existing));
		}		
		return "success";
	}
	
	public String execute()
	{
		Book book = linkController.get(getBookId());
		
		if(book !=  null)
		{
			this.author = book.getAuthor();
			this.title = book.getTitle();
			this.genre = book.getGenre();
			this.blurb = book.getBlurb();
			this.isbn = book.getIsbn();
			
			return "update";
		}
		else
		{
			return "add";
		}
		
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getBlurb() {
		return blurb;
	}

	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
}
