package com.jcasey.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jcasey.model.Book;
import com.jcasey.model.Genre;

@Entity
@Table(name="BookGenre")
public class BookGenre {
	
	private Long id;
	public void setId(Long id) {
		this.id = id;
	}
	@Id
	@GeneratedValue
	@Column(name="book_genre_id")
	public Long getId() {
		return id;
	}
	private Book book;
	@ManyToOne
	@JoinColumn(name="book_id")
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	private Genre genre;

	@ManyToOne
	@JoinColumn(name="genre_id")
	public Genre getGenre() {
		return genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	// constructor
	public BookGenre(){}
	public BookGenre(Book book, Genre genre){
		this.book = book;
		this.genre = genre;
	}
}
