package com.jcasey.model;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Genre")
public class Genre {
	// fields
	private Long id;
	public void setId(Long id) {
		this.id = id;
	}

	private String genre;
	// constructor
	public Genre(){
	}
	public Genre(String genre){
		this.genre = genre; 
	}	
	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	private List <BookGenre> bookGenre;

	@Id
	@GeneratedValue
	@Column(name="genre_id")
	public Long getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		Genre other = (Genre) obj;
		return genre.equals(other.getGenre());
	}
}
