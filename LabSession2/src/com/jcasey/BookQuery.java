package com.jcasey;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//import oracle.jdbc.pool.OracleDataSource;
import org.hsqldb.jdbc.JDBCDataSource;

import org.apache.commons.lang3.StringUtils;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;


public class BookQuery extends ActionSupport{
	private static final String BOOK_ID = "BOOK_ID";
	private static final String TITLE = "TITLE";
	private static final String AUTHOR = "AUTHOR";
	private static final String GENRE = "GENRE";
	private static final  String ISBN = "ISBN";
	private static final  String BLURB = "BLURB";
	
	private String genre;
	private String title;
	private String author;
	
	private boolean fuzzy = false;	

	ArrayList <Book> books = new ArrayList<Book>();
	List<String> genreList;
	
	public List<String> getGenreList() {
		return genreList;
	}

	public void setGenreList(List<String> genreList) {
		this.genreList = genreList;
	}

	public BookQuery()
	{
		setGenreList();
	}
	
	private void setGenreList()
	{
		genreList = new ArrayList<String>();
		genreList.add("Select genre...");
		// get genre names
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			JDBCDataSource ds = new JDBCDataSource();
			
			ds.setUrl("jdbc:hsqldb:hsql://localhost/");
			
			// set other data source properties
			ds.setPassword("");
			ds.setUser("SA");
			
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			String querySQL = "select distinct GENRE from book"; 
						
			System.err.println(querySQL);
			
			rs = stmt.executeQuery(querySQL);
			
			// build up the output from the result set
			// keep iterating through the result set while another row exists
			
			while(rs.next() == true)
			{
				genreList.add(rs.getString(GENRE));
			}
		}
		catch (SQLException e)
		{
		}
		finally
		{

			if(rs != null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(stmt != null)
			{
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null)
			{
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	class Book
	{
		public Book() {

		}
		private int bookId;
		private String title;
		private String author;
		private String genre;
		private String isbn;
		private String blurb;
		
		public int getBookId() {
			return bookId;
		}
		public void setBookId(int bookId) {
			this.bookId = bookId;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public String getGenre() {
			return genre;
		}
		public void setGenre(String genre) {
			this.genre = genre;
		}
		public String getIsbn() {
			return isbn;
		}
		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}
		public String getBlurb() {
			return blurb;
		}
		public void setBlurb(String blurb) {
			this.blurb = blurb;
		}
	}

	
	public String query()
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			System.out.println(genre);
			System.out.println(title);
			System.out.println(author);
			
			//OracleDataSource ds = new OracleDataSource();
			JDBCDataSource ds = new JDBCDataSource();
			
			// setup URL according to Oracle's specs
			//ds.setURL("jdbc:oracle:thin:@docoraclere.unitec.ac.nz:1521:students");
			ds.setUrl("jdbc:hsqldb:hsql://localhost/");
			
			// set other data source properties
			//ds.setPassword("TEST");
			//ds.setUser("JCASEY");
			ds.setPassword("");
			ds.setUser("SA");
			
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			String querySQL = "select * from book"; 
			
			if(StringUtils.isNotBlank(title) || StringUtils.isNotBlank(author) || StringUtils.isNotBlank(genre))
			{
				querySQL = querySQL + " where";
			}
			// enable the fuzzy search
			String ope = " = ";
			String sign = "";
			if (fuzzy)
			{
				ope = " like ";
				sign = "%";
			}			
			
			if(StringUtils.isNotBlank(title))
			{
				querySQL = querySQL + " title"+ope+"'"+sign+title+sign+"'";
			}
			if(StringUtils.isNotBlank(author))
			{
				querySQL = querySQL + " author"+ope+"'"+sign+author+sign+"'";
			}
			if(StringUtils.isNotBlank(genre))
			{
				querySQL = querySQL + " genre"+ope+"'"+sign+genre+sign+"'";
			}
			
			
			System.err.println(querySQL);
			
			rs = stmt.executeQuery(querySQL);
			
			// build up the output from the result set
			// keep iterating through the result set while another row exists
			
			while(rs.next() == true)
			{
				Book book = new Book();
				
				book.setBookId(rs.getInt(BOOK_ID));
				book.setTitle(rs.getString(TITLE));
				book.setAuthor(rs.getString(AUTHOR));
				book.setGenre(rs.getString(GENRE));
				book.setIsbn(rs.getString(ISBN));
				book.setBlurb(rs.getString(BLURB));
				
				books.add(book);
			}
			
			return Action.SUCCESS;
		}
		catch (SQLException e)
		{
			return Action.ERROR;
		}
		finally
		{

			if(rs != null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(stmt != null)
			{
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null)
			{
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	@Override
	public void validate() {
		// to escape the first validation
		if (title == null && author == null && genre == null)
		{
			return;
		}
		// validation for parameters
		if ((title.trim().isEmpty()) &&
			(author.trim().isEmpty()) &&
			(genre.trim().isEmpty()))
		{
			super.addFieldError("title", "put at least one criteria.");
			super.addFieldError("author", "put at least one criteria.");
			super.addFieldError("genre", "put at least one criteria.");
		}
	}


	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public ArrayList<Book> getBooks() {
		return books;
	}


	public void setBooks(ArrayList<Book> books) {
		this.books = books;
	}

	public boolean isFuzzy() {
		return fuzzy;
	}

	public void setFuzzy(boolean fuzzy) {
		this.fuzzy = fuzzy;
	}
}
