package com.jcasey.controller;


import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.jcasey.model.BookGenre;
import com.jcasey.model.Book;
import com.jcasey.model.Genre;
import com.jcasey.util.HibernateUtil;

public class BookGenreManager extends HibernateUtil {

	public BookGenre add(BookGenre bookGenre) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(bookGenre);
		session.getTransaction().commit();
		return bookGenre;
	}

	public BookGenre delete(BookGenre bookGenre) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.delete(bookGenre);
		session.getTransaction().commit();
		return bookGenre;
	}

	public List<BookGenre> findByBook(Book book){
		return findByBookId(book.getBookId());
	}
	public List<BookGenre> findByBookId(long book_id){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from BookGenre where book_id = :id");
		query.setParameter("id", book_id);
		return query.list();
	}
	public BookGenre findByBookAndGenre(Book book, Genre genre){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from BookGenre where book_id = :bid and genre_id = :gid");
		query.setParameter("bid", book.getBookId());
		query.setParameter("gid", genre.getId());
		List list = query.list();
		if (list.size() == 0){
			return null;
		}else{
			return (BookGenre)list.get(0);
		}
	}
}
