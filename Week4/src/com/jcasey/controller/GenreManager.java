package com.jcasey.controller;


import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.jcasey.model.Genre;
import com.jcasey.util.HibernateUtil;

public class GenreManager extends HibernateUtil {

	public Genre add(Genre genre) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(genre);
		session.getTransaction().commit();
		return genre;
	}	
	public Genre findByGenre(String genre){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Genre where genre = :genre");
		query.setParameter("genre", genre);
		List list = query.list();
		if (list.size() == 0){
			return null;
		}else{
			return (Genre)list.get(0);
		}	
	}
	
	public Genre delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Genre genre = (Genre)session.load(Genre.class, id);
		if(null != genre) {
			session.delete(genre);
		}
		session.getTransaction().commit();
		return genre;
	}
}
