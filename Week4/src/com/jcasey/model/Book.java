package com.jcasey.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.jcasey.model.BookGenre;
import com.jcasey.model.Genre;


@Entity
@Table(name="Book")
public class Book implements Serializable
{
	private static final long serialVersionUID = -6837174276610847586L;
	private Long bookId;
	private String title;
	private String author;
	private String genre;
	private String isbn;
	private String blurb;
	
	private Set<BookGenre> bookGenres;

	@OneToMany(mappedBy="book")
	public Set<BookGenre> getBookGenres() {
		return bookGenres;
	}
	public void setBookGenres(Set<BookGenre> bookGenres) {
		this.bookGenres = bookGenres;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="book_id")
	public Long getBookId() {
		return bookId;
	}
	@Column(name="title")
	public String getTitle() {
		return title;
	}
	@Column(name="author")
	public String getAuthor() {
		return author;
	}
	//@Column(name="genre")
	public String getGenre() {
		//return genre;
		ArrayList<String> genres = new ArrayList<String>();
		if (getBookGenres() != null){
			for (BookGenre bg : getBookGenres()){
				genres.add(bg.getGenre().getGenre());
			}
		}
		if (genres.size() > 0){
			return StringUtils.join(genres, ",");
		}
		else {
			return "";
		}
	}
	@Column(name="isbn")
	public String getIsbn() {
		return isbn;
	}
	@Column(name="blurb")
	public String getBlurb() {
		return blurb;
	}
	
	
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
}