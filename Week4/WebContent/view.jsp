<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>

<title>Struts 2 - Book Database</title>
</head>

<body>
<h2>Struts 2 - Book Database</h2>
<h4>Are you sure to update?</h4>
<s:actionerror />
<s:form action="commit.action" method="post">
	
	<s:textfield name="bookId" id="ab" label="Id" size="20" readonly="true" />
	<s:textfield name="title" key="label.title" size="20" value="%{title}" readonly="true" />
	<s:textfield name="author" key="label.author" size="20" readonly="true"  />
	<s:textfield name="genre" key="label.genre" size="20"  readonly="true" />
	<s:textfield name="blurb" label="Blurb" size="20"  readonly="true" />
	<s:textfield name="isbn" label="isbn" size="20"  readonly="true" />

	<s:submit name="View" method="execute" label="Update" align="right" type="button"/>
</s:form>

</body>
</html>
