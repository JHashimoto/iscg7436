package com.jcasey.action;

import com.jcasey.controller.BookManager;
import com.jcasey.model.Book;
import com.opensymphony.xwork2.Action;

public class ViewBook
{	
	private BookManager linkController;
	
	private Long bookId;
	private String title;
	private String author;
	private String genre;
	private String isbn;
	private String blurb;
	
	public ViewBook()
	{
		linkController = new BookManager();
	}
	
	public String execute()
	{
		return "success";		
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getBlurb() {
		return blurb;
	}

	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}
}
