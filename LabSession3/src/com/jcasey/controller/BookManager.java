package com.jcasey.controller;


import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.jcasey.model.Book;
import com.jcasey.util.HibernateUtil;

public class BookManager extends HibernateUtil {

	public Book update(Book book) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.saveOrUpdate(book);
		session.getTransaction().commit();
		return book;
	}
	
	public Book add(Book book) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(book);
		session.getTransaction().commit();
		return book;
	}
	public Book delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Book book = (Book) session.load(Book.class, id);
		if(null != book) {
			session.delete(book);
		}
		session.getTransaction().commit();
		return book;
	}

	public Book get(Long id) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		LinkedList<Book> books = new LinkedList<Book>();
		try {
			Criteria criteria = session.createCriteria(Book.class);
			criteria.add(Restrictions.eq("bookId", id));
			
			books.addAll((List<Book>)criteria.list());
			
		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.getTransaction().commit();
		
		if(!books.isEmpty())
		{
			return books.getFirst();
		}
		else
		{
			return null;
		}
	}

	public List<Book> list(String genre, String title, String author) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		//List<Book> books = null;
		LinkedList<Book> books = new LinkedList<Book>();
	try {
		
		Criteria criteria = session.createCriteria(Book.class);
		
		if ((title != null) && (!title.isEmpty())){
			criteria.add(Restrictions.like("title","%"+title+"%"));	
		}
		if ((author != null) && (!author.isEmpty())){
			criteria.add(Restrictions.like("author","%"+author+"%"));			
		}
		if ((genre != null) && (!genre.isEmpty())){
			criteria.add(Restrictions.like("genre","%"+genre+"%"));			
		}
		// get the list
		books.addAll((List<Book>)criteria.list());
		
		// do the same thing using HQL
		String qSmnt = "from Book b ";
		boolean titleAdded = false;
		boolean authorAdded = false;
		boolean genreAdded = false;
		if ((title != null) && (!title.isEmpty())){
			//qSmnt = qSmnt.concat("where b.title = :title ");
			qSmnt = qSmnt.concat("where b.title like :title ");
			titleAdded = true;
		}
		if ((author != null) && (!author.isEmpty())){
			if (titleAdded){
				qSmnt = qSmnt.concat(" and ");
			}
			else{
				qSmnt = qSmnt.concat(" where ");
			}
				
			//qSmnt = qSmnt.concat(" b.author = :author ");
			qSmnt = qSmnt.concat(" b.author like :author ");
			authorAdded = true;
		}
		if ((genre != null) && (!genre.isEmpty())){
			if (titleAdded || authorAdded){
				qSmnt = qSmnt.concat(" and ");
			}
			else{
				qSmnt = qSmnt.concat(" where ");
			}
			//qSmnt = qSmnt.concat(" b.genre = :genre ");
			qSmnt = qSmnt.concat(" b.genre like :genre ");
			genreAdded = true;
		}
		//Query query = session.createQuery("from Book b where b.genre = :genre");
		//query.setString("genre", genre);
		Query query = session.createQuery(qSmnt);
		if (titleAdded) query.setString("title", title);
		if (authorAdded)query.setString("author", author);
		if (genreAdded) query.setString("genre", genre);
		
		String act = query.getQueryString();
		if (act != null) act = "";
		
	} catch (HibernateException e) {
		e.printStackTrace();
		session.getTransaction().rollback();
	}
	session.getTransaction().commit();
	return books;
	}
}
