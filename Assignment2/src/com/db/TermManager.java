package com.db;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.classic.Session;

import com.model.Term;

public class TermManager {
	// Singleton instance
	private static TermManager instance;
	private TermManager(){}
	public static TermManager getInstance(){
		if (instance == null){
			instance = new TermManager();
		}
		return instance;
	}

	public Term add(Term term) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try{
			session.save(term);
			session.getTransaction().commit();
		}
		catch(Exception ex){
		}
		return term;
	}
	public Term findByTerm(String term) {		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Term where term = :term");
		query.setParameter("term", term);
		List list = query.list();
		if (list.size() == 0){
			return null;
		}else{
			return (Term)list.get(0);
		}
	}
}
