package com.db;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.classic.Session;

import com.model.Posting;;

public class PostingManager {
	// Singleton instance
	private static PostingManager instance;
	private PostingManager(){}
	public static PostingManager getInstance(){
		if (instance == null){
			instance = new PostingManager();
		}
		return instance;
	}

	public Posting add(Posting posting) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try{
			session.save(posting);
			session.getTransaction().commit();
		}
		catch(Exception ex){
		}
		return posting;
	}
	public List<Posting> findByTerm(int term_id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Posting where term_id = :term_id");
		query.setParameter("term_id", term_id);
		return query.list();
	}
	public List<Posting> findByDocAndTerm(int doc_id, int term_id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery(
				"from Posting where doc_id = :doc_id and term_id = :term_id");
		query.setParameter("doc_id", doc_id);
		query.setParameter("term_id", term_id);
		return query.list();
	}
}
