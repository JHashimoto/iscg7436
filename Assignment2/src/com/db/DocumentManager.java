package com.db;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.model.Document;

public class DocumentManager {
	// Singleton instance
	private static DocumentManager instance;
	private DocumentManager(){}
	public static DocumentManager getInstance(){
		if (instance == null){
			instance = new DocumentManager();
		}
		return instance;
	}

	public Document add(Document doc) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try{
			session.save(doc);
			session.getTransaction().commit();
		}
		catch(Exception ex){
		}
		return doc;
	}
	public Document update(Document doc) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.saveOrUpdate(doc);
		session.getTransaction().commit();
		return doc;
	}
	public Document delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Document student = (Document) session.load(Document.class, id);
		if(null != student) {
			session.delete(student);
		}
		session.getTransaction().commit();
		return student;
	}
	public List<Document> getAll() {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		//List<Book> books = null;
		List<Document> students = new LinkedList<Document>();
	try {
		
		Criteria criteria = session.createCriteria(Document.class);
		
		students.addAll((List<Document>)criteria.list());
		
		// do the same thing using HQL
		String qSmnt = "from Student order by student_id";
		
		Query query = session.createQuery(qSmnt);
		
		String act = query.getQueryString();
		if (act != null) act = "";
		
	} catch (HibernateException e) {
		e.printStackTrace();
		session.getTransaction().rollback();
	}
	session.getTransaction().commit();
	return students;
	}
	public Document find(int id) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		LinkedList<Document> documents = new LinkedList<Document>();
		try {
			Criteria criteria = session.createCriteria(Document.class);
			criteria.add(Restrictions.eq("Doc_Id", id));
			
			documents.addAll((List<Document>)criteria.list());
			
		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.getTransaction().commit();
		
		if(!documents.isEmpty())
		{
			return documents.getFirst();
		}
		else
		{
			return null;
		}
	}
	public Document findByURL(String url) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Document where url = :url");
		query.setParameter("url", url);
		List list = query.list();
		if (list.size() == 0){
			return null;
		}else{
			return (com.model.Document)list.get(0);
		}
	}
}
