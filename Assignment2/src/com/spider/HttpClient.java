package com.spider;

import java.net.URI;

public interface HttpClient {
	public boolean get(URI request, StringBuilder messageResponse);
	public boolean get(URI request, StringBuilder messageResponse, StringBuilder title);
}
