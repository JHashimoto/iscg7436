package com.spider;

import java.net.URI;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;

import com.db.DocumentManager;
import com.lucene.*;

public class Spider extends Thread{
	
	private Semaphore sem;
	
	private HttpClient client;
	private HttpParser parser;
	private Indexer indexer;
	private Searcher searcher;
	
	private URI uri;
	private int depth;
	
	//public static List<Spider> AllThreads = new LinkedList<Spider>();
	private static Set<URI> alreadyVisited = new HashSet<URI>();

	public Spider(HttpClient client, HttpParser parser, Indexer indexer, Searcher searcher)
	{
		this(client, parser, indexer, searcher, null, -1, null);
	}	
	public Spider(HttpClient client, HttpParser parser, Indexer indexer, Searcher searcher, URI uri, int depth, Semaphore sem)
	{
		super(SpiderController.SpiderGroup, "");
		this.client = client;
		this.parser = parser;
		this.indexer = indexer;
		this.searcher = searcher;
		this.uri = uri;
		this.depth = depth;
		this.sem = sem;
	}
	
	@Override
	public void run() {
		System.out.println(getName());
		this.crawl(uri, depth, sem);
	}

	public void crawl(URI uri, int depth, Semaphore sem){
		if (depth <= 0) return;
		if (alreadyVisited.contains(uri)) return;
				
		this.uri = uri;
		this.depth = depth;
		this.sem = sem;
		
		StringBuilder messageResponse = new StringBuilder();
		
		StringBuilder title = new StringBuilder();
		try {
			sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (client.get(uri, messageResponse, title)){
			alreadyVisited.add(uri);
			// create the index
			indexer.createIndex(title.toString(), messageResponse.toString(), uri.toASCIIString());
			List<URI> uris = parser.parseLinks(uri.toASCIIString(), messageResponse.toString(), client);
			List<Spider> threads = new LinkedList<Spider>();
			for (URI request : uris){
				threads.add(new Spider(client, parser, indexer, searcher, request, (depth-1), sem));
				//crawl(request, (depth-1));
			}
			for (Spider thread : threads){
				thread.start();
			}
			sem.release();
		}
	}
	public List<com.model.Document> search(String term){
		List<com.model.Document> docs = new LinkedList<com.model.Document>();
		List<URI> results = searcher.search(indexer, term);
		for (URI uri: results){
			DocumentManager manager = DocumentManager.getInstance();
			docs.add(manager.findByURL(uri.toASCIIString()));			
		}		
		return docs;
	}
}
