package com.spider;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ClassPathResource;

import com.model.Document;

public class SpiderController {
	private Spider webCrawler;
	public static ThreadGroup SpiderGroup = new ThreadGroup("SpiderGroup");
	// depth
	public static final int DEFAULT_DEPTH = 15;
	public static final int SEMAPHORE_PERMISSION = 3;
	// Singleton Constructor
	private static SpiderController instance = null;
	private SpiderController(){}
	public static SpiderController getInstance(){
		if (instance == null){
			instance = new SpiderController();
		}
		return instance;
	}
	/**
	 * Entry point
	 * @param args[0]=URI as a starting point
	 * @param args[1]=term for searching
	 * @param args[2]=depth (option)
	 */
	public static void main(String[] args)
	{
		String uri = args[0];
		String term = args[1];
		int depth = DEFAULT_DEPTH;
		if (args.length > 2){
			// if the depth is specified
			depth = Integer.parseInt(args[2]);
		}
		
		List<Document> results = new LinkedList<Document>();
		try {
			SpiderController.getInstance().doCrawl(new URIBuilder(uri).build(), depth);
			results.addAll(SpiderController.getInstance().search(term));
			for (Document doc : results){
				System.out.print(doc.getTitle());
				System.out.print(": ");
				System.out.println(doc.getUrl());
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}	
	}
	
	public void doCrawl(URI uri){
		doCrawl(uri, DEFAULT_DEPTH);
	}
	public void doCrawl(URI uri, int depth){
		DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
        reader.loadBeanDefinitions(new ClassPathResource("spring.xml"));
        
        Semaphore semaphore = new Semaphore(SEMAPHORE_PERMISSION);

		webCrawler = (Spider) beanFactory.getBean("Spider");
		webCrawler.crawl(uri, depth, semaphore);
	}
	public List<Document> search(String term){
		boolean isRunning = true;
		while(isRunning){
			if (SpiderGroup.activeCount() == 0){
				isRunning = false;
			}
		}
		return webCrawler.search(term);
	}
}
