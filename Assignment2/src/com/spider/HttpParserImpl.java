package com.spider;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HttpParserImpl implements HttpParser{

	private final int TIMEOUT = 10*1000;

	@Override
	public List<URI> parseLinks(final String host, final String content){
		return this.parseLinks(host, content, null);
	}
	@Override
	public List<URI> parseLinks(final String host, final String content, HttpClient client){
		final List<URI> urls = new ArrayList <URI> ();
		Document doc = null;
		try {
			if (client != null){
				doc = ((HttpClientImpl)client).getDocument();
			}else{
				Connection conn = Jsoup.connect(host)
									   .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0")
									   .timeout(TIMEOUT);
				if (conn == null){
					System.err.println("connection if failed.");
					return urls;
				}
				int statusCode = conn.execute().statusCode();
				if (statusCode == 200){
					doc = conn.get();
				}else{
					System.err.println("Received error code: " + statusCode);
				}
			}
			Elements links = doc.select("a[href]");
			for (Element link : links){
				try {
					urls.add(new URI(link.attr("abs:href")));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
		} catch (MalformedURLException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			return urls;
		}
		return Collections.unmodifiableList(urls);
	}
}
