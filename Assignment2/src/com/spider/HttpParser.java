package com.spider;

import java.net.URI;
import java.util.List;

public interface HttpParser {
	public List<URI> parseLinks(String host, String content);
	public List<URI> parseLinks(String host, String content, HttpClient client);
}
