package com.spider;

import java.awt.SystemColor;
import java.io.IOException;
import java.net.URI;
import java.net.UnknownHostException;

import org.jsoup.*;
import org.jsoup.nodes.Document;

public class HttpClientImpl implements HttpClient
{
	private final int TIMEOUT = 10*1000;
	private Document doc;
	public Document getDocument(){ return doc;}
	
	public HttpClientImpl(){}
	
	@Override
	public boolean get(URI uri, StringBuilder response)
	{
		return get(uri, response, new StringBuilder());
	}
	@Override
	public boolean get(URI uri, StringBuilder response, StringBuilder title)
	{
		try {
			Connection conn = Jsoup.connect(uri.toString())
								   .timeout(TIMEOUT);
			if (conn == null){
				System.err.println("connection if failed.");
				return false;
			}
			int statusCode = conn.execute().statusCode();
			if (statusCode == 200){
				doc = conn.get();
				response.append(doc.body());
				title.append(doc.title());
			}else{
				System.err.println("Received error code: " + statusCode);
			}
		}catch (HttpStatusException ex){
			ex.printStackTrace();
		}catch (UnknownHostException ex){
			ex.printStackTrace();
		}catch (IllegalArgumentException ex){
			ex.printStackTrace();
		}catch (UnsupportedMimeTypeException ex){
			ex.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
