package com.lucene;

import java.io.IOException;

import org.apache.http.client.utils.URIBuilder;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import com.db.*;
import com.model.*;

public class SearcherImpl implements Searcher {

	public List<URI> search(Indexer indexer, String term){
		List<URI> results = new LinkedList<URI>();
		IndexWriter writer = ((IndexerImpl)indexer).getWriter();
		try {
			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Analyzer analyzer = ((IndexerImpl)indexer).getAnalyzer();
		QueryParser parser = new QueryParser(Version.LUCENE_45, "contents", analyzer);
		try {
			int hitsPerPage = 10;
			Query query = parser.parse(term);
			Directory index = ((IndexerImpl)indexer).getIndex();
			IndexReader reader = DirectoryReader.open(index);
		    IndexSearcher searcher = new IndexSearcher(reader);
		    TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
		    searcher.search(query, collector);
		    ScoreDoc[] hits = collector.topDocs().scoreDocs;
		    // Store to the database
		    TermManager tManager = TermManager.getInstance();
		    Term t = tManager.findByTerm(term);
		    if (t == null){
		    	t = new Term();
		    	t.setTerm(term);
		    	tManager.add(t);
		    }
		    PostingManager pManager = PostingManager.getInstance();
		    for (Posting posting : pManager.findByTerm(t.getTerm_id())){
		    	URI uri = new URIBuilder(posting.getDocument().getUrl()).build();
		    	results.add(uri);
		    }
		    if (hits.length == 0){
		    	return results;
		    }
		    for (ScoreDoc hit : hits){
		    	int docID = hit.doc;
		    	Document doc = searcher.doc(docID);
				URIBuilder builder = new URIBuilder(doc.get("url"));
				URI uri = builder.build();
				if (results.contains(uri)) continue;
				results.add(uri);
				// Store to the database
				addPosting(term, doc.get("url"));
		    }
		    return results;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e){
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return results;
	}
	private void addPosting(String term, String url){
		com.model.Posting posting = new Posting();
		posting.setTerm(TermManager.getInstance().findByTerm(term));
		posting.setDocument(DocumentManager.getInstance().findByURL(url));
		PostingManager.getInstance().add(posting);
	}
}
