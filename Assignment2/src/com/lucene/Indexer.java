package com.lucene;

public interface Indexer {
	public void createIndex(String title, String contents, String url);
}
