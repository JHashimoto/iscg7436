package com.lucene;

import java.net.URI;
import java.util.List;

public interface Searcher {
	public List<URI> search(Indexer indexer, String term);
}
