package com.lucene;

import java.io.IOException;
import java.io.File;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

import com.db.DocumentManager;

public class IndexerImpl implements Indexer {
	// properties
	private StandardAnalyzer analyzer;
	public Analyzer getAnalyzer() {
		return analyzer;
	}
	//private Directory index = new RAMDirectory();
	File dir;
	private Directory index;
	
	public Directory getIndex() {
		return index;
	}
	private IndexWriterConfig config;
    private IndexWriter writer;	
	
	public IndexWriter getWriter() {
		return writer;
	}
	// Singleton Instance
	private static IndexerImpl instance;
	private IndexerImpl(){
		analyzer = new StandardAnalyzer(Version.LUCENE_45);
	    config = new IndexWriterConfig(Version.LUCENE_45, analyzer);
	    try {
		    index = new RAMDirectory();
			writer = new IndexWriter(index, config);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static IndexerImpl getInstance(){
		if (instance == null){
			instance = new IndexerImpl();
		}
		return instance;
	}
	@Override
	public void createIndex(String title, String contents, String url) {
		if (writer == null) return;
		try {
			// Store data to the database
			DocumentManager dManager = DocumentManager.getInstance();
			synchronized (dManager) {
				com.model.Document mDoc = dManager.findByURL(url);
				if (mDoc == null){
					mDoc = new com.model.Document();
					mDoc.setTitle(title);
					mDoc.setUrl(url);
					dManager.add(mDoc);
				}				
			}
			addDoc(writer, title, contents, url);
		} catch (IOException e) {
			e.printStackTrace(); 
		}
	}
	private void addDoc(IndexWriter writer, String title, String contents, String url)
			throws IOException {
	  Document doc = new Document();
	  doc.add(new StringField("title", title, Field.Store.YES));
	  doc.add(new TextField("contents", contents, Field.Store.YES));
	  doc.add(new StringField("url", url, Field.Store.YES));
	  writer.addDocument(doc);
	}
}
