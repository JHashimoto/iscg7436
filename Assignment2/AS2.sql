CREATE TABLE Document(Doc_Id int primary key, Title varchar(100), URL varchar(2083));
CREATE TABLE Term(Term_Id int primary key, Term varchar(500));
CREATE TABLE Posting(Posting_Id int IDENTITY PRIMARY KEY, Doc_Id int, Term_Id int, FOREIGN KEY (Doc_Id) REFERENCES Document (Doc_Id) ON DELETE CASCADE, FOREIGN KEY (Term_Id) REFERENCES Term (Term_Id)  ON DELETE CASCADE);
