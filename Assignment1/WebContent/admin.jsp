<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC
 "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="./css/style.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Marks Assistant</title>
</head>
<body>
<section>
<font size="6">Marks Assistant</font>
</br>
<font size="5">ISCG7436 - Assignment 1</font>
</section>
<section>
Manage Students and Criteria data by uploading or downloading XML Format file. 
<div>
</br>
<font size="4">Download:</font></br>
<form action="xmlmanager" method="GET">
	Target:
	<select name="target">
		<option>Student</option>
		<option>Criteria</option>
	</select>
	</br>
	<input type="submit" value="Download">
</form>
</br>
<font size="4">Upload:</font></br>
<form action="xmlmanager" method="POST" enctype="multipart/form-data">
	Target:
	<select name="target">
		<option>Student</option>
		<option>Criteria</option>
	</select>
	</br>
	<input type="file" name="file">
	</br>
	<input type="submit" value="Upload">
</form>
</div>
</section>
<a href="./initialise.jsp">Return</a>
</body>
</html>