<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC
 "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
var criteriaIdArray = [];
function initialise()
{
	initSelector();
	initCriteriaIdArray();
	calculateTotal();
	createBody();
}
function initSelector(){
	currentId = document.getElementById('StdId').value;
	if (currentId <= "0") return;
	
	var options = document.getElementById('StudentSelector').options;
	for (var i = 0; i < options.length; i++){
		if (options.item(i).value.split("/")[1] == currentId){
			options.item(i).selected = true;
			break;
		}			
	}
}
function initCriteriaIdArray(){
	criteriaIdArray.length = 0;
	var tbl = document.getElementById('scoreTable');
		
	for (var i=0; i < tbl.rows.length; i++){
		if (tbl.rows[i].id == null) continue;
		if (tbl.rows[i].id.length == 0) continue;
		criteriaIdArray.push(tbl.rows[i].id);
	}
}
function syncScore(byRange, id){
	var cId = id.split("_")[1];
	if (byRange){
		document.getElementById("actual_"+cId).value =
			document.getElementById(id).value;
	}else{
		document.getElementById("range_"+cId).value =
			document.getElementById(id).value;		
	}
}
function changeStudent(std){
	cId = std.split("/")[1];
	document.getElementById('StudentSelectForm').action =
		document.getElementById('StudentSelectForm').action + "?currentId=" + cId;
	document.getElementById('StudentSelectForm').submit();
}
function createBody(){
	currentId = document.getElementById('StdId').value;
	if (currentId <= "0") return;

	var builder = "";
	var name = document.getElementById('StudentSelector').value.split("/")[0];
	builder += "Dear " + name;	
	builder += "\n";
	builder += "\n";
	builder += "marks for assignment1";	
	builder += "\n";	
	builder += "\n";

	var number = 1;
	var skipped = false;

	for (var i=0; i < criteriaIdArray.length; i++){
		if (!document.getElementById('check_'+criteriaIdArray[i]).checked) continue;
		builder += (number++) + ".";
		builder += document.getElementById('cname_'+criteriaIdArray[i]).value;
		builder += " : ";
		builder += document.getElementById('actual_'+criteriaIdArray[i]).value;
		builder += "/";
		builder += document.getElementById('possible_'+criteriaIdArray[i]).value;
		builder += " - ";
		builder += document.getElementById('comment_'+criteriaIdArray[i]).value;
		
		if (skipped) skipped = false;
		else builder += "\n";
	}	
	builder += "\n";
	builder += "Total ";
	builder += document.getElementById('actualTotal').value;
	builder += "/";
	builder += document.getElementById('possibleTotal').value;
	builder += " - ";
	builder += document.getElementById('overall').value;
	
	document.getElementById('body').value = builder;
}
function calculateTotal(){
	var actual = 0;
	var possible = 0;
	var tempActual = 0;
	var tempPossible = 0;

	for (var i=0; i < criteriaIdArray.length; i++){
		if (!document.getElementById('check_'+criteriaIdArray[i]).checked) continue;
		tempActual = parseInt(document.getElementById('actual_'+criteriaIdArray[i]).value);
		tempPossible = parseInt(document.getElementById('possible_'+criteriaIdArray[i]).value);
		actual += tempActual;
		possible += tempPossible;
		document.getElementById('score_'+criteriaIdArray[i]).value = (tempActual/tempPossible).toFixed(2);
	}
	document.getElementById('actualTotal').value = actual;
	document.getElementById('possibleTotal').value = possible;
	document.getElementById('overall').value = getOverallComment(actual, possible);
}
function getOverallComment(actual, possible){
	currentId = document.getElementById('StdId').value;
	if (currentId <= "0") return;
	var score = (actual/possible)*100;
	var comment = "";
	
	if (score > 90){
		comment = "Excellent!";
	}else if (score > 80){
		comment = "Very good!";
	}else if (score > 60){
		comment = "Good!";
	}else if (score > 40){
		comment = "Not very good";
	}else if (score > 20){
		comment = "Not good";
	}else {
		comment = "Bad";
	}
	return comment;
}
function setParameter(){
	var param = location.href.split("?")[1];
	document.getElementById('SendEmail').action += "?" + param;
}
</script>
<link rel="stylesheet" href="./css/style.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Marks Assistant</title>
</head>
<body onload="initialise();">
<s:hidden id="StdId" value="%{currentId}"/>
<section>
<font size="6">Marks Assistant</font>
</br>
<font size="5">ISCG7436 - Assignment 1</font>
<div align="right">
</div>
</section>
	<section>
	<s:form id="StudentSelectForm" action="ChangeStudent.action" method="post">
		<s:select id="StudentSelector" list="students" onchange="changeStudent(value)"/>
	</s:form>
		<s:hidden key="currentId" value="%{currentId}"/>
		<table id="scoreTable" width="100%" border="1">
		  <tr>
		    <td>Criteria</td>
		    <td>Marks</td>
		    <td>Actual/Possible</td>
		    <td>Comments</td>
		    <td>Score</td>
		    <td>&nbsp;</td>
		  </tr>
		  <s:iterator value="cManager.getAll()">
			<s:form action="UpdateScore.action?currentId=%{currentId}&criteriaId=%{criteriaId}">
		  <tr id=<s:property value="%{criteriaId}"/>>
		    <td>
		    	<input id=<s:property value="%{'check_'+criteriaId}"/> value=<s:property value="%{criteriaName}"/> name="check" type="checkbox" onclick="calculateTotal();createBody();" checked/>
		    	<s:hidden id="%{'cname_'+criteriaId}"  value="%{criteriaName}"></s:hidden>
    			<s:property id="%{'criteria_'+criteriaId}" value="criteriaName" />
   			</td>
		    <td><input id=<s:property value="%{'range_'+criteriaId}"/> name="range" type="range" min="0" max=<s:property value="%{possibleScore}"/>
		    		value=<s:property value="getActualScore(criteriaId)"/> 
		    			style="width:95%;" onchange="syncScore(true, id);calculateTotal();createBody();"/>
   			</td>
		    <td>
		    	<s:textfield id="%{'actual_'+criteriaId}" size="2" name="actual" key="actual" value="%{getActualScore(criteriaId)}"
		    		onchange="syncScore(false, id);calculateTotal();createBody(); " />
		    	<s:textfield id="%{'possible_'+criteriaId}" size="2" name="possible" value="%{possibleScore}" readonly="true" />
		    </td>
		    <td>
		    	<s:textfield id="%{'comment_'+criteriaId}" cssStyle="width:98%" name="comment" key="comment"
		    		value="%{sManager.find(currentId, criteriaId).comment}" onchange="createBody();"/>
		    </td>
		    <td><input id=<s:property value="%{'score_'+criteriaId}"/>  name="score"
		    			size="3" type="text" value="0" readonly />
   			</td>
		    <td><s:submit name="save" label="Update" align="center" type="button" onclick="createBody();" /></td>
		  </tr>
		  	</s:form>
		  </s:iterator>
		  <tr>
		    <td>&nbsp;</td>
		    <td align="right">Total:</td>
		    <td>
		    	<s:textfield id="actualTotal" size="2" readonly="readonly" />		    	
		    	<s:textfield id="possibleTotal" size="2" readonly="readonly" />
		    </td>
		    <td>
		    	<input id="overall" style="width:98%" onchange="createBody();" />
		    </td>
		  </tr>
		</table>
		<br/>
	</section>
	<section>
	<form id="SendEmail" method="post" action="/Assignment1/EmailSender">
		Email:
		<input type="text" name="address" value=<s:property value="%{stManager.find(currentId).getEmail()}"/> />
		<input type="submit" value="Send Email" align="right" onclick="setParameter();" />
		<br/>
		<textarea id="body" name="body" rows="10" cols="110" ></textarea>
	</form>
	</section>
<div align="left">
	<a href="./admin.jsp">Administrator</a>
</div>
</body>
</html>