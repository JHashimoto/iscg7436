CREATE TABLE Student(Student_Id int primary key, Student_Name varchar(30), Email varchar(50));

CREATE TABLE Criteria(criteria_id int IDENTITY PRIMARY KEY, criteria_name varchar(30), possible_score int);

CREATE TABLE Score(score_id int IDENTITY PRIMARY KEY, student_id int, criteria_id int, score int, FOREIGN KEY (student_id) REFERENCES Student (student_id) ON DELETE CASCADE, FOREIGN KEY (criteria_id) REFERENCES Criteria (criteria_id)  ON DELETE CASCADE);

insert into Student values (12345,'Jun Hashimoto','jhashiinnz@gmail.com');
insert into Student values (56789,'Adam','adam@unitec.ac.nz');
insert into Student values (998877,'Becky','becky@unitec.ac.nz');

insert into Criteria values (1, 'System Implementation', 60);
insert into Criteria values (2, 'Program Documentation', 10);
insert into Criteria values (3, 'Testing and Results', 5);
insert into Criteria values (4, 'Project Reporting', 15);
insert into Criteria values (5, 'Final Presentation', 10);

