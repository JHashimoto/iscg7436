package com.as1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name="Criteria")
public class Criteria implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long criteriaId;
	private String criteriaName;
	private long possibleScore;
	@XmlTransient
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="criteria_id")
	public long getCriteriaId() {
		return criteriaId;
	}
	public void setCriteriaId(long criteriaId) {
		this.criteriaId = criteriaId;
	}
	@Column(name="criteria_name")
	public String getCriteriaName() {
		return criteriaName;
	}
	public void setCriteriaName(String criteriaName) {
		this.criteriaName = criteriaName;
	}
	@Column(name="possible_score")
	public long getPossibleScore() {
		return possibleScore;
	}
	public void setPossibleScore(long possibleScore) {
		this.possibleScore = possibleScore;
	}

}
