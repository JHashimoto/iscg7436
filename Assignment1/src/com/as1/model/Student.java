package com.as1.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name="Student")
public class Student implements Serializable
{
	private static final long serialVersionUID = 1L;
	private Long studentId;
	private String name;
	private String email;
	
	private Set<Score> scoreSet;

	@XmlTransient
	@OneToMany(mappedBy="student")
	public Set<Score> getScoreSet() {
		return scoreSet;
	}
	public void setScoreSet(Set<Score> scoreSet) {
		this.scoreSet = scoreSet;
	}
	@Id
	@Column(name="student_id")
	public Long getStudentId() {
		return studentId;
	}
	@Column(name="Student_Name")
	public String getName() {
		return name;
	}
	@Column(name="Email")
	public String getEmail() {
		return email;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setEmail(String email) {
		this.email = email;
	}	
}
