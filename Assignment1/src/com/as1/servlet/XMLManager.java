package com.as1.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.as1.controller.*;
import com.as1.model.*;
import com.as1.xmltemplate.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

@WebServlet("/xmlmanager")
@MultipartConfig
public class XMLManager extends HttpServlet{
	private static final long serialVersionUID = 1L;
	// XML File download method
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String target = req.getParameter("target");
		if (target == null){ return;}
	    resp.setContentType("application/octet-stream");
	    resp.setHeader("Content-Disposition"
	         , "attachment; filename=\"" + target + ".xml\"");
	    PrintWriter writer = resp.getWriter();
	    this.marshal(target, writer);
	    writer.flush();
	}
	
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }
    // Upload
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
    	Part part = request.getPart("file");
    	InputStream stream = part.getInputStream();
    	
		String target = request.getParameter("target");
		unmarshal(target, stream);
		
		response.sendRedirect("admin.jsp");
    }
    
    // Marshaling methods
	public void marshal(String target, PrintWriter writer){
		if (target.equalsIgnoreCase("Student")){
			marshalStudents(writer);
		}else if (target.equalsIgnoreCase("Criteria")){
			marshalCriteria(writer);
		}
	}	
	private void marshalStudents(PrintWriter writer){
		StudentTemplate template = new StudentTemplate();
		template.setStudents(StudentManager.getInstance().getAll());		
		JAXB.marshal(template, writer);
	}
	private void marshalCriteria(PrintWriter writer){
		CriteriaTemplate template = new CriteriaTemplate();
		template.setCriteriaList(CriteriaManager.getInstance().getAll());
		JAXB.marshal(template, writer);
	}
	
    // Unmarshaling methods
	public void unmarshal(String target, InputStream xml){
		if (target.equalsIgnoreCase("Student")){
			unmarshalStudents(xml);
		}else if (target.equalsIgnoreCase("Criteria")){
			unmarshalCriteria(xml);
		}
	}
	private void unmarshalStudents(InputStream xml){
		StudentTemplate template = JAXB.unmarshal(xml, StudentTemplate.class);
		List<Long> idList = new ArrayList<Long>();
		for (Student st : template.getStudents()){
			Student exist = StudentManager.getInstance().find(st.getStudentId());
			if (exist == null){
				StudentManager.getInstance().add(st);
			} else {
				StudentManager.getInstance().update(st);
			}
			idList.add(st.getStudentId());
		}
		for (Student st : StudentManager.getInstance().getAll()){
			if (!idList.contains(st.getStudentId())){
				StudentManager.getInstance().delete(st.getStudentId());
			}
		}
	}
	private void unmarshalCriteria(InputStream xml){
		CriteriaTemplate template = JAXB.unmarshal(xml, CriteriaTemplate.class);
		List<String> nameList = new ArrayList<String>();
		for (com.as1.model.Criteria ct : template.getCriteriaList()){
			com.as1.model.Criteria exist = CriteriaManager.getInstance().findByName(ct.getCriteriaName());
			if (exist == null){
				CriteriaManager.getInstance().add(ct);
			} else {
				exist.setCriteriaName(ct.getCriteriaName());
				exist.setPossibleScore(ct.getPossibleScore());
				CriteriaManager.getInstance().update(exist);
			}
			nameList.add(ct.getCriteriaName());
		}
		for (com.as1.model.Criteria ct : CriteriaManager.getInstance().getAll()){
			if (!nameList.contains(ct.getCriteriaName())){
				CriteriaManager.getInstance().deleteByName(ct.getCriteriaName());
			}
		}
	}
}
