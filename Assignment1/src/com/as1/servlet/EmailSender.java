package com.as1.servlet;

import java.io.*;
import java.util.Properties;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServlet;

import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import java.net.*;

public class EmailSender extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "465");
		
		props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", "465");

        final Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("iscg7436@gmail.com", "enterprisejavaprogramming");
            }
        });
		
        String to = req.getParameter("address");
        String body = req.getParameter("body");
        
		MimeMessage mMsg = new MimeMessage(session);
		
		try{
			mMsg.setRecipients(Message.RecipientType.TO, to);
			InternetAddress ia = new InternetAddress("iscg7436@unitec.ac.nz", "Lecturer");
			mMsg.setFrom(ia);
			
			mMsg.setSubject("ISCG 7436 Assignment1 Marks");
			mMsg.setText(body);
			Transport.send(mMsg);
		} catch (Exception e){
		}
		try {
			resp.sendRedirect(new URI(req.getHeader("referer")).toString());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}