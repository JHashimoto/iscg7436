package com.as1.action;

import java.util.*;

import com.as1.model.*;
import com.as1.controller.*;

public class ModifyScore {
	// Constructor
	public ModifyScore(){
		this.setStudentNames();
	}
	// Current Student ID
	private long currentId;	
	public long getCurrentId() {
		return currentId;
	}
	public void setCurrentId(long currentId) {
		this.currentId = currentId;
	}
	// Student name List
	private List<String> students = new ArrayList<String>();	
	public List<String> getStudents() {
		return students;
	}
	public void setStudents(List<String> students) {
		this.students = students;
	}
	// Score List
	private List<Score> scoreList = new ArrayList<Score>();	
	public List<Score> getScoreList() {
		return scoreList;
	}
	public void setScoreList(List<Score> scoreList) {
		this.scoreList = scoreList;
	}
	// Student Manager
	private StudentManager stManager = StudentManager.getInstance();	
	public StudentManager getStManager() {
		return stManager;
	}
	public void setStManager(StudentManager stManager) {
		this.stManager = stManager;
	}
	// Criteria Manager
	private CriteriaManager cManager = CriteriaManager.getInstance();
	public CriteriaManager getcManager() {
		return cManager;
	}
	public void setcManager(CriteriaManager cManager) {
		this.cManager = cManager;
	}
	// Score Manager
	private ScoreManager sManager = ScoreManager.getInstance();
	public ScoreManager getsManager() {
		return sManager;
	}
	public void setsManager(ScoreManager sManager) {
		this.sManager = sManager;
	}
	public String execute() throws Exception {
		//this.students = StudentManager.getInstance().getAll();
		return "success";
	}
	private void setStudentNames(){
		this.students.add("Select student...");
		for (Student st : StudentManager.getInstance().getAll()){
			this.students.add(st.getName() + "/" + ((Long)st.getStudentId()).toString());
		}
	}
	public String changeStudent(){
		int l = this.getcManager().getAll().size();
		if (l < 0){ return "";}
		if (this.currentId < 0) { return "";}
		this.scoreList.clear();
		this.scoreList.addAll(ScoreManager.getInstance().findByStudentId(this.currentId));
		
		return "success";
	}
	public long getActualScore(long criteriaId){
		Score s = sManager.find(currentId, criteriaId);
		if (s == null) return 0;
		return s.getScore();
	}
	public String updateScore(){
		Score score = sManager.find(currentId, criteriaId);
		if (score == null){
			score = addScore();
		}
		score.setScore(actual);
		score.setComment(comment);
		sManager.update(score);
		return "success";
	}
	public Score addScore(){
		Score score = new Score();
		score.setStudent(stManager.find(currentId));
		score.setCriteria(cManager.find(criteriaId));
		return sManager.add(score);
	}
	private long criteriaId;	
	public long getCriteriaId() {
		return criteriaId;
	}
	public void setCriteriaId(long criteriaId) {
		this.criteriaId = criteriaId;
	}
	private long actual;
	public long getActual() {
		return actual;
	}
	public void setActual(long actual) {
		this.actual = actual;
	}
	private String comment = "";
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
