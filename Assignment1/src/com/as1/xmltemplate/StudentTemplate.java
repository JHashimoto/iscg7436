package com.as1.xmltemplate;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import com.as1.model.Student;

public class StudentTemplate {
	private List<Student> students;

	@XmlElementWrapper
	@XmlElement(name="student")
	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}	
}
