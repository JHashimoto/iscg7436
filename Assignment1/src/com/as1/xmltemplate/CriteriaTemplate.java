package com.as1.xmltemplate;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;


public class CriteriaTemplate {
	private List<com.as1.model.Criteria> criteriaList;

	@XmlElementWrapper
	@XmlElement(name="criteria")
	public List<com.as1.model.Criteria> getCriteriaList() {
		return criteriaList;
	}

	public void setCriteriaList(List<com.as1.model.Criteria> criteriaList) {
		this.criteriaList = criteriaList;
	}	
}
