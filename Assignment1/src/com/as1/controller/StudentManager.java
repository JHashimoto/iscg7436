package com.as1.controller;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.as1.model.Student;
import com.as1.util.HibernateUtil;

public class StudentManager extends HibernateUtil {
	// Singleton
	private static StudentManager instance = null;
	public StudentManager() {}
	public static StudentManager getInstance(){
		if (StudentManager.instance == null){
			StudentManager.instance = new StudentManager();
		}
		return StudentManager.instance;
	}
	public Student add(Student student) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(student);
		session.getTransaction().commit();
		return student;
	}
	public Student update(Student student) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.saveOrUpdate(student);
		session.getTransaction().commit();
		return student;
	}
	public Student delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Student student = (Student) session.load(Student.class, id);
		if(null != student) {
			session.delete(student);
		}
		session.getTransaction().commit();
		return student;
	}

	public List<Student> getAll() {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		//List<Book> books = null;
		List<Student> students = new LinkedList<Student>();
	try {
		
		Criteria criteria = session.createCriteria(Student.class);
		
		students.addAll((List<Student>)criteria.list());
		
		// do the same thing using HQL
		String qSmnt = "from Student order by student_id";
		
		Query query = session.createQuery(qSmnt);
		
		String act = query.getQueryString();
		if (act != null) act = "";
		
	} catch (HibernateException e) {
		e.printStackTrace();
		session.getTransaction().rollback();
	}
	session.getTransaction().commit();
	return students;
	}

	public Student find(Long id) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		LinkedList<Student> students = new LinkedList<Student>();
		try {
			Criteria criteria = session.createCriteria(Student.class);
			criteria.add(Restrictions.eq("studentId", id));
			
			students.addAll((List<Student>)criteria.list());
			
		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.getTransaction().commit();
		
		if(!students.isEmpty())
		{
			return students.getFirst();
		}
		else
		{
			return null;
		}
	}
}
