package com.as1.controller;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.as1.util.HibernateUtil;

public class CriteriaManager extends HibernateUtil {
	// Singleton
	private static CriteriaManager instance = null;
	public CriteriaManager() {}
	public static CriteriaManager getInstance(){
		if (CriteriaManager.instance == null){
			CriteriaManager.instance = new CriteriaManager();
		}
		return CriteriaManager.instance;
	}
	public com.as1.model.Criteria add(com.as1.model.Criteria criteria) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(criteria);
		session.getTransaction().commit();
		return criteria;
	}
	public com.as1.model.Criteria update(com.as1.model.Criteria criteria) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.saveOrUpdate(criteria);
		session.getTransaction().commit();
		return criteria;
	}
	public com.as1.model.Criteria delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		com.as1.model.Criteria criteria = (com.as1.model.Criteria) session.load(com.as1.model.Criteria.class, id);
		if(null != criteria) {
			session.delete(criteria);
		}
		session.getTransaction().commit();
		return criteria;
	}
	public com.as1.model.Criteria deleteByName(String name) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		com.as1.model.Criteria criteria = this.findByName(name);
		if(null != criteria) {
			session.delete(criteria);
		}
		session.getTransaction().commit();
		return criteria;
	}

	public List<com.as1.model.Criteria> getAll() {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<com.as1.model.Criteria> criteriaList = new LinkedList<com.as1.model.Criteria>();
	try {
		
		Criteria criteria = session.createCriteria(com.as1.model.Criteria.class);
		
		criteriaList.addAll((List<com.as1.model.Criteria>)criteria.list());
		
		// do the same thing using HQL
		String qSmnt = "from Criteria";
		
		Query query = session.createQuery(qSmnt);
		
		String act = query.getQueryString();
		if (act != null) act = "";
		
	} catch (HibernateException e) {
		e.printStackTrace();
		session.getTransaction().rollback();
	}
	session.getTransaction().commit();
	return criteriaList;
	}
	public com.as1.model.Criteria findByName(String name){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Criteria where criteria_name = :name");
		query.setParameter("name", name);
		List list = query.list();
		if (list.size() == 0){
			return null;
		}else{
			return (com.as1.model.Criteria)list.get(0);
		}
	}
	public com.as1.model.Criteria find(Long id) {		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		LinkedList<com.as1.model.Criteria> criteriaList = new LinkedList<com.as1.model.Criteria>();
		try {
			Criteria criteria = session.createCriteria(com.as1.model.Criteria.class);
			criteria.add(Restrictions.eq("criteriaId", id));
			
			criteriaList.addAll((List<com.as1.model.Criteria>)criteria.list());
			
		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		session.getTransaction().commit();
		
		if(!criteriaList.isEmpty())
		{
			return criteriaList.getFirst();
		}
		else
		{
			return null;
		}
	}
}
