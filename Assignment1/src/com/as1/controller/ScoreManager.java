package com.as1.controller;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.classic.Session;

import com.as1.model.Score;
import com.as1.util.HibernateUtil;

public class ScoreManager extends HibernateUtil {
	// Singleton
	private static ScoreManager instance = null;
	public ScoreManager() {}
	public static ScoreManager getInstance(){
		if (ScoreManager.instance == null){
			ScoreManager.instance = new ScoreManager();
		}
		return ScoreManager.instance;
	}
	public Score add(Score score) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(score);
		session.getTransaction().commit();
		return score;
	}
	public Score update(Score score) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.saveOrUpdate(score);
		session.getTransaction().commit();
		return score;
	}
	public List<Score> findByStudentId(long stdId){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Score where student_id = :id");
		query.setParameter("id", stdId);
		return query.list();
	}
	
	public Score find(long stdId, long criteriaId){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery(
				"from Score where student_id = :sid and criteria_id = :cid");
		query.setParameter("sid", stdId);
		query.setParameter("cid", criteriaId);
		List list = query.list();
		if (list.size() == 0){
			return null;
		}else{
			return (Score)list.get(0);
		}
	}
}
